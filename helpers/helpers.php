<?php

if (!function_exists('sportily_route')) {
    function sportily_route($name, $parameters = [], $absolute = true) {
        $result = '';

        if ($absolute && isset(app('request')->__prefix)) {
            $result .= '/' . app('request')->__prefix;
        }

        $result .= route($name, $parameters, false);
        return $result;
    }
}

if (!function_exists('routeToTeam')) {
    function routeToTeam($entry) {
        $division_entry_id = isset($entry['division_entry_id']) ? $entry['division_entry_id'] : $entry['id'];
        return isset($entry['team_id'])
            ? sportily_route('team', [ $entry['team_id'], $division_entry_id ])
            : '#';
    }
}

if (!function_exists('excerpt')) {
    function excerpt($content, $length = 100) {
        $offset = strlen($content) > $length ? strpos($content, '.', $length) + 1 : strlen($content);
        return substr($content, 0, $offset);
    }
}
