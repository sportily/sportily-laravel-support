<?php

return [

    // used by FixedOrganisationId middleware.
    //'organisation' => 42,

    // used by SubdomainOrganisationId and PathOrganisationId middlewares.
    /*'organisations' => [
        'foo' => 42,
        'bar' => 43
    ]*/

    /* 'secure-envs => ['prod', 'production']' */
    /* 'forceHttp' => ['live'] */

    // Used with DatabaseHostBasedOrganisationId
    /* 'domain' => env('SPORTILY_LEAGUE_DOMAIN', '.sporti.ly') */

];
