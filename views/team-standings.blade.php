@extends('layouts.main')

@section('content')

    @include('includes.divisions', ['route' => 'team-standings'])

    <div class="full fixtures">
        <h3>
            Team Standings
            @include('includes.division')
        </h3>

        @if (isset($division_id))
            <table class="table standings">
                <thead>
                    <tr>
                        <th class="index aux">&nbsp;</th>
                        <th class="title">Team</th>
                        <th class="played">P</th>
                        <th class="won">W</th>
                        <th class="drawn">D</th>
                        <th class="lost">L</th>
                        <th class="lost"><abbr title="Forfeits">F</abbr></th>
                        <th class="ga">GA</th>
                        <th class="pts">Pts</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($entries as $index => $entry)
                        <tr>
                            <td class="index aux">{{ $index + 1 }}.</td>
                            <td class="title">
                                <a class="basic" href="{{ routeToTeam($entry) }}">{{ $entry['name'] }}</a>
                            </td>
                            <td class="played">{{ $entry['stats']['played'] }}</td>
                            <td class="won">{{ $entry['stats']['won'] }}</td>
                            <td class="drawn">{{ $entry['stats']['drawn'] }}</td>
                            <td class="lost">{{ $entry['stats']['lost'] }}</td>
                            <td class="ga">{{ $entry['stats']['forfeited'] }}</td>
                            <td class="ga">{{ $entry['stats']['goals_against'] }}</td>
                            <td class="pts">{{ $entry['stats']['points'] }}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        @else
            <p>You must choose a division to see team standings.</p>
        @endif
    </div>

@endsection
