@if (isset($entry['logo']))
    <span class="fixture_logo logo" style="background-image: url({{ $entry['logo']['file']['uri'] }})"></span>
@else
    <span class="fixture_logo fixture_nologo logo"></span>
@endif
