<script src="/components/angular/angular.js"></script>
<script src="/components/lodash/dist/lodash.min.js"></script>
<script src="/components/restangular/dist/restangular.min.js"></script>
<script src="/components/moment/min/moment-with-locales.min.js"></script>
<script src="/components/sportily-angular-api/dist/js/sportily.api.min.js?rel=@@timestamp"></script>

<script>
    var module = angular.module('app.core', ['sportily.api']);

    module.config(function(SportilyApiProvider) {
        SportilyApiProvider.setAccessToken('{{ $access_token->value }}');
    });

    @if (App::isLocal())
        module.config(function(SportilyApiProvider) {
            SportilyApiProvider.setBaseUrl('http://localhost:9000');
        });
    @endif
</script>

@if (App::isLocal())
    <script src="//localhost:9501/livereload.js"></script>
@endif
