@if (isset($division_id))

    <span>| {{ $divisions->get($division_id)['name'] }}</span>

@elseif (isset($allowAll) && $allowAll)

    <span>| All</span>

@endif
