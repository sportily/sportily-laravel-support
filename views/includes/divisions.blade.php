<ul>
    @foreach ($divisions as $division)
        <li>
            <a href="{{ sportily_route($route, [$division['id']])}}">
                {{ $division['name'] }}
            </a>
        </li>
    @endforeach
</ul>
