<div class="fixture">
    <div class="fixture_body">
        <div class="fixture_home">
            @include('includes/logo', [ 'entry' => $fixture['home_entry'] ])
            <div class="fixture_name">
                @if (isset($fixture['home_entry']))
                    <a class="basic" href="{{ routeToTeam($fixture['home_entry']) }}">
                        {{ $fixture['home_entry']['name'] }}
                    </a>
                @else
                    &nbsp;
                @endif
            </div>
            <div class="fixture_info">
                {{ (new DateTime($fixture['start_time']))->format(isset($format) ? $format : 'g:ia') }}
            </div>
            @if (isset($fixture['home_entry']))
                @include('snippets.fixture-score', [ 'entry' => $fixture['home_entry'] ])
            @endif
        </div>
        <div class="fixture_vs">-</div>
        <div class="fixture_away">
            @include('includes/logo', [ 'entry' => $fixture['away_entry'] ])
            <div class="fixture_name">
                @if (isset($fixture['away_entry']))
                    <a class="basic" href="{{ routeToTeam($fixture['away_entry']) }}">
                        {{ $fixture['away_entry']['name'] }}
                    </a>
                @else
                    &nbsp;
                @endif
            </div>
            @if (isset($fixture['division']))
                <div class="fixture_info">
                    {{ $fixture['division']['name'] }}
                </div>
            @endif
            @if (isset($fixture['away_entry']))
                @include('snippets.fixture-score', [ 'entry' => $fixture['away_entry'] ])
            @endif
        </div>
    </div>
    @if ($fixture['status'] == 'complete')
        <div class="fixture_link">
            <a href="{{ sportily_route('report', ['fixture_id' => $fixture['id']]) }}">Match Report</a>
        </div>
        <div class="fixture_button">
            <a class="button button-outline" href="{{ sportily_route('report', ['fixture_id' => $fixture['id']]) }}">Match Report</a>
        </div>
    @elseif ($fixture['status'] == 'in_progress')
        <div class="fixture_link">
            <a href="{{ sportily_route('report', ['fixture_id' => $fixture['id']]) }}">Follow Live</a>
        </div>
        <div class="fixture_button">
            <a class="button button-outline" href="{{ sportily_route('report', ['fixture_id' => $fixture['id']]) }}">Follow Live</a>
        </div>
    @endif
</div>
