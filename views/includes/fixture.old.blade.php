<span class="fixture {{ !isset($responsive) || $responsive ? 'responsive' : '' }} {{ isset($class) ? $class : ''}}">
    <span class="home">
      @if (isset($fixture['home_entry']))
        @include('includes/logo', [ 'entry' => $fixture['home_entry'] ])
      @endif
        <span class="details">
            <span class="info">
                <span class="team">
                    @if (isset($fixture['home_entry']))
                        <a class="basic" href="{{ routeToTeam($fixture['home_entry']) }}">
                            {{ $fixture['home_entry']['name'] }}
                        </a>
                    @else
                        &nbsp;
                    @endif
                </span>
                @if (isset($fixture['home_entry']))
                  @include('snippets.fixture-score', [ 'entry' => $fixture['home_entry'] ])
                @endif
            </span>
            <span class="extra">{{ (new DateTime($fixture['start_time']))->format(isset($format) ? $format : 'g:ia') }}</span>
        </span>
    </span>
    <span class="vs">{{ isset($vs) ? $vs : '-'}}</span>
    <span class="away">
      @if (isset($fixture['away_entry']))
        @include('includes/logo', [ 'entry' => $fixture['away_entry'] ])
      @endif

        <span class="details">
            <span class="info">
                <span class="team">
                    @if (isset($fixture['away_entry']))
                        <a class="basic" href="{{ routeToTeam($fixture['away_entry']) }}">
                            {{ $fixture['away_entry']['name'] }}
                        </a>
                    @else
                        &nbsp;
                    @endif
                </span>
                @if (isset($fixture['away_entry']))
                  @include('snippets.fixture-score', [ 'entry' => $fixture['away_entry'] ])
                @endif
            </span>
            @if (isset($fixture['division']))
                <span class="extra">{{ $fixture['division']['name'] }}</span>
            @endif
        </span>
    </span>
    @if ($fixture['status'] == 'complete')
        <span class="report">
            <a href="{{ sportily_route('report', ['fixture_id' => $fixture['id']]) }}">Match Report</a>
        </span>
    @endif
</span>
