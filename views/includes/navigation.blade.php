<section class="navigation">
    <h3>&nbsp;</h3>
    <ul>
        <li><a href="{{sportily_route('home')}}">Home</a></li>
        <li><a href="{{sportily_route('posts')}}">News</a></li>
        <li>
            <ul>
                <li><a href="{{sportily_route('fixtures')}}">Fixtures</a></li>
                <li><a href="{{sportily_route('results')}}">Results</a></li>
                <li><a href="{{sportily_route('team-standings')}}">Team Standings</a></li>
                <li><a href="{{sportily_route('player-stats')}}">Player Stats</a></li>
            </ul>
            <a href="#">League <i class="icon-caret-right"></i></a>
        </li>
        <li>
            <ul>
                <li><a href="{{sportily_route('photos')}}">Photos</a></li>
                <li><a href="{{sportily_route('documents')}}">Documents &amp; Forms</a></li>
                <li><a href="{{sportily_route('links')}}">Links</a></li>
            </ul>
            <a href="#">Media <i class="icon-caret-right"></i></a>
        </li>
        <li><a href="{{sportily_route('contact-us')}}">Contact Us</a></li>
        <li><a href="{{sportily_route('registration')}}">Registration</a></li>
        <li><a href="http://admin.sporti.ly">Admin</a></li>

        @if ($profile != null)

            <li><a href="{{sportily_route('logout')}}">Logout</a></li>

        @endif

    </ul>
</section>
