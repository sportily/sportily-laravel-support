<!doctype html>
<html lang="en" class="page-home variant-{$config->value('theme.website.varient')}">

    <head>
        <title>BIPHA | British Inline Puck Hockey Association</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10, user-scalable=1">

        <!-- Our own CSS -->
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}">

        <!-- Font Awesome -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />

        <script src="/components/xdomain/dist/xdomain.min.js"></script>
        <script>
          xdomain.slaves({
                '{{ env('SPORTILY_BASE_URL') }}': '/xdomain-proxy.html'
          });
        </script>
        <!-- Typekit -->
        <script type="text/javascript" src="//use.typekit.net/ohk2dbk.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    </head>

    <body class="variant-{{ $organisation['id'] }}">

        <div class="container">

            @include('includes.header')

            @include('includes.navigation')

            <section class="main">@yield('content')</section>

            @include('includes.footer')

            @yield('scripts')

        </div>

    </body>

</html>
