@extends('layouts.main')

@section('content')
    <h3>Contact Us</h3>
    <div class="panel">
        <table>
            <tr>
              <th>Position</th>
              <th>Name</th>
              <th>Email</th>
            </tr>
            @foreach ($contacts as $contact)

                <tr>
                    <td>{{$contact['position']}}</td>
                    <td>{{$contact['given_name'] }} {{ $contact['family_name'] }} </td>
                    <td><a href="mailto:{{ $contact['email']}}">{{ $contact['email']}}</a></td>
                </tr>

            @endforeach

        </table>
    </div>
@endsection
