@extends('layouts.main')

@section('content')

    @foreach ($posts as $post)

        <div class="panel">
            <h3>{{ $post['headline'] }}</h3>
            <p>
                Published on {{ (new DateTime($post['created_at']))->format('F j') }}
                | <a href="{{ sportily_route('post', ['id' => $post['id']]) }}">Read in full &rarr;</a>
            </p>
        </div>

    @endforeach

@endsection
