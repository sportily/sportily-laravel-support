@if (isset($entry['result']))

    @if ($entry['result']['type'] == 'standard/scores')
        <span class="fixture_score">{{ $entry['result']['score'] }}</span>

    @elseif ($entry['result']['type'] == 'forfeit/against')
        <span class="fixture_score"><span class="danger">F</span></span>

    @elseif ($entry['result']['type'] == 'forfeit/for')
        <span class="fixture_score"><span class="danger">5</span></span>

    @endif

@elseif (isset($entry['score']))
    <span class="fixture_score">{{ $entry['score'] }}</span>

@endif
