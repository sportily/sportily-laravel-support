<div class="fixtures panel">
    <h3>
        Fixtures
        @if ($fixtures)
            <span>| {{ (new DateTime($fixtures->first()['start_time']))->format('F j') }}</span>
        @endif
    </h3>
    <ul>

        @if (count($fixtures) > 0)

            @foreach ($fixtures as $fixture)

                @if ($fixture['status'] == 'not_started')
                    <li>
                        @include('includes.fixture', ['fixture' => $fixture])
                    </li>
                @endif

            @endforeach

        @else

            <p>There are no upcoming fixtures to show!</p>

        @endif

    </ul>
</div>
