<div class="results panel">
    <h3>
        Results
        @if ($results)
            <span>| {{ (new DateTime($results->first()['start_time']))->format('F j') }}</span>
        @endif
    </h3>
    <ul>

        @if (count($results) > 0)

            @foreach ($results as $result)

                <li>
                    @include('includes.fixture', ['fixture' => $result])
                </li>

            @endforeach

        @else

            <p>There are no recent results to show!</p>

        @endif

    </ul>
</div>
