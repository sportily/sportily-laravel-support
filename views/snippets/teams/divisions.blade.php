@if (count($team_entries) > 1)

    <div class="panel">
        <h3>Divisions</h3>
        <ul class="basic">

            @foreach ($team_entries as $entry)

                <li>
                    <a class="basic" href="{{ routeToTeam($entry) }}">
                        {{ $divisions->lookup[$entry['division_id']]['name'] }}
                    </a>
                </li>

            @endforeach

        </ul>
    </div>

@endif
