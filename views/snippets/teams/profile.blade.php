<div class="panel">
    <h3>
        {{ $team['name'] }}
        <span>| {{ $division['name'] }}</span>
    </h3>
    <div class="team-profile">
        @if (array_key_exists('logo_image_id', $club) && $club['logo_image_id'] != null)
            <div class="team-profile__logo">
                <img src="{{ $club['logo_image']['file']['uri'] }}"/>
            </div>
        @endif
        <p>
            @if (array_key_exists('bio', $club))
                {{ excerpt($club['bio'], 100) }}
            @endif
        </p>
        <p class="team-profile__links">
            @if (array_key_exists('website', $club))
            Website: <a class="link" target="_blank" href="{{ $club['website'] }}">{{ $club['website'] }}</a>
            @endif
        </p>
    </div>
</div>
