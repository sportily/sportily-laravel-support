<div class="panel">
    <h3>Team Stats</h3>
    <ul class="stats-tabs">
        <li class="stat">
            <em>{{ $stats['best_result']['gf'] }} – {{ $stats['best_result']['ga'] }}</em>
            <small>best result</small>
        </li>
        <li class="stat">
            <em>{{ number_format($stats['win_percentage'], 0) }}%</em>
            <small>win percentage</small>
        </li>
        <li class="stat">
            <em>{{ number_format($stats['goal_average'], 1) }}</em>
            <small>goal average</small>
        </li>
        <li class="stat">
            <em>{{ $stats['clean_sheets'] }}</em>
            <small>clean sheets</small>
        </li>
    </ul>
</div>

<div class="panel">
    <h3>Team Standings</h3>
    <table class="table standings">
        <thead>
            <tr>
                <th class="title">Team</th>
                <th class="played">P</th>
                <th class="won">W</th>
                <th class="drawn">D</th>
                <th class="lost">L</th>
                <th class="lost"><abbr title="Forfeits">F</abbr></th>
                <th class="ga">GD</th>
                <th class="pts">Pts</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($entries as $index => $entry)
                <tr>
                    <td class="title">
                        <a class="basic" href="{{ routeToTeam($entry) }}">
                            {{ $entry['name'] }}
                        </a>
                    </td>
                    <td class="played">{{ $entry['stats']['played'] }}</td>
                    <td class="won">{{ $entry['stats']['won'] }}</td>
                    <td class="drawn">{{ $entry['stats']['drawn'] }}</td>
                    <td class="lost">{{ $entry['stats']['lost'] }}</td>
                    <td class="ga">{{ $entry['stats']['forfeited'] }}</td>
                    <td class="ga">{{ $entry['stats']['goal_diff'] }}</td>
                    <td class="pts">{{ $entry['stats']['points'] }}</td>
                </tr>
            @endforeach

        </tbody>
    </table>

</div>
