@if (count($results))

    <div class="panel fixtures fixtures-narrow">
        <h3>Results</h3>
        <ul>

            @foreach ($results as $result)

                <li>
                    @include('includes.fixture', ['fixture' => $result, 'format' => 'M j, g:ia', 'responsive' => false])
                </li>

            @endforeach

        </ul>
    </div>

@endif
