@if (count($fixtures))

    <div class="panel fixtures fixtures-narrow">
        <h3>Fixtures</h3>
        <ul>

            @foreach ($fixtures as $fixture)

                <li>
                    @include('includes.fixture', ['fixture' => $fixture, 'format' => 'M j, g:ia', 'responsive' => false])
                </li>

            @endforeach

        </ul>
    </div>

@endif
