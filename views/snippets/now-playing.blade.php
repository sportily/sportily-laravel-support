@if ($in_progress)

    <div class="panel">
        <h3>Live Game Centre</h3>
        <ul>
            <li>
                @include('includes.fixture', ['fixture' => $in_progress])
            </li>
        </ul>
        <!--<a href="/live" class="btn btn-block">Watch Now!</a>-->
    </div>

@endif
