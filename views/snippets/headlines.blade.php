<div class="news panel">
    <h3>News <span>| Latest</span></h3>

    <ul class="basic">
        @foreach ($posts as $post)
            <li>
                <a href="{{ sportily_route('post', ['id' => $post['id']]) }}" class="basic">
                    {{ $post['headline'] }}
                    <span>{{ (new DateTime($post['created_at']))->format('F j') }}</span>
                </a>
            </li>
        @endforeach
        <li><a href="{{ sportily_route('post') }}">News Archives →</a></li>
    </ul>
</div>
