@extends('layouts.main')

@section('content')

    <h3>
        {{ $post['headline'] }}
        <span>| {{ (new DateTime($post['created_at']))->format('F j') }}</span>
    </h3>

    {!! $post['html'] !!}

@endsection
