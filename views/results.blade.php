@extends('layouts.main')

@section('content')

    @include('includes.divisions', ['route' => 'results', 'allowAll' => true])

    <div class="full results">
        <h3>
            Results
            @include('includes.division', ['allowAll' => true])
        </h3>

        @if (count($results))

            @foreach ($results as $date => $date_results)

                <div class="panel">
                    <h4>{{ (new DateTime($date))->format('F j') }}</h4>
                    <ul>

                        @foreach ($date_results as $result)

                            <li>
                                @include('includes.fixture', ['fixture' => $result])
                            </li>

                        @endforeach

                    </ul>
                </div>

            @endforeach

        @else

            <p>There are no results to show!</p>

        @endif

    </div>

@endsection
