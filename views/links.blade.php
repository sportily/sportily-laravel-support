@extends('layouts.main')

@section('content')
    <h3>Links</h3>
    @if (count($links))
      <ul>
      @foreach ($links as $link)
        <li><a href="{{ $link['location'] }}">{{ $link['text'] }}</a></li>
      @endforeach
      </ul>
    @else
      <p>No links to display.</p>
    @endif
@endsection
