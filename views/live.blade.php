@extends('layouts.main')

@section('content')
    <h3>Live</h3>

    @if ($organisation['id'] == 13)
        <div class="live-stream">
            <div class="aspect-16-9">
                <iframe width="100%" height="100%"
                    src="//www.ustream.tv/embed/4195740?html5ui"
                    scrolling="no"
                    frameborder="0"
                    style="position:absolute; top:0; left: 0">
                </iframe>
            </div>
        </div>
    @endif

    <div id="live"
        data-base="{{ config('sportily-api.base_url') }}"
        data-access-token="{{ $access_token->value }}"
        data-pusher-token="{{ config('sportily-live.pusher_token') }}"
        data-organisation-id="{{ $organisation['id'] }}">
    </div>

@endsection

@section('scripts')
    <script src="https://js.pusher.com/3.2/pusher.min.js"></script>
    <script type="text/javascript" src="{{ elixir('js/live.js') }}"></script>
    @if (App::isLocal())
        <script src="//localhost:9501/livereload.js"></script>
    @endif
@endsection
