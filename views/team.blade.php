@extends('layouts.main')

@section('content')

    <div class="sidebar">

        @include('snippets.teams.divisions')

        @include('snippets.teams.fixtures')

        @include('snippets.teams.results')

    </div>

    <div class="primary">

        @include('snippets.teams.profile')

        @include('snippets.teams.standings')

        @include('snippets.teams.stats')

    </div>

@endsection
