@extends('layouts.main')

@section('content')

<div class="col col3-4" ng-app="app" sportily-fixture="{{ $fixture['id'] }}">

    <h1>Now Playing</h1>

    <div class="fixturehdr">
        <div class="fixturehdr__home">
            <h2>{{ $fixture['home_entry']['name'] }}</h2>
            <small>{{ Carbon\Carbon::parse($fixture['start_time'])->format('g:ia, F j') }}</small>
        </div>
        <div class="fixturehdr__away">
            <h2>{{ $fixture['away_entry']['name'] }}</h2>
            <small>{{ $venue['name'] }}</small>
        </div>
        <sportily-scores class="fixturehdr__scores"></sportily-scores>
    </div>

    <sportily-timeline></sportily-timeline>

</div>

@include('includes.angular')
<link rel="stylesheet" href="{{ asset('components/sportily-angularfire-fixture/dist/css/sportily.fixture.css') }}"/>
<script src="{{ asset('components/firebase/firebase.js') }}"></script>
<script src="{{ asset('components/angularfire/dist/angularfire.min.js') }}"></script>
<script src="{{ asset('components/sportily-angularfire-fixture/dist/js/sportily.fixture.js?rel=@@timestamp') }}"></script>
<script src="{{ asset('components/sportily-angularfire-fixture/dist/js/sportily.fixture.templates.js?rel=@@timestamp') }}"></script>
<script>angular.module('app', ['app.core', 'sportily.fixture']);</script>

@endsection
