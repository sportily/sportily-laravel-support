@extends('layouts.main')

@section('content')
    <h3>Match Report</h3>
    <div id="live"
        data-base="{{ config('sportily-api.base_url') }}"
        data-access-token="{{ $access_token->value }}"
        data-pusher-token="{{ config('sportily-live.pusher_token') }}"
        data-organisation-id="{{ $organisation['id'] }}"
        data-fixture-id="{{ $fixture_id }}">
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ elixir('js/live.js') }}"></script>
    @if (App::isLocal())
        <script src="//localhost:9501/livereload.js"></script>
    @endif
@endsection
