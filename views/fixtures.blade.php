@extends('layouts.main')

@section('content')

    @include('includes.divisions', ['route' => 'fixtures', 'allowAll' => true])

    <div class="full fixtures">
        <h3>
            Fixtures
            @include('includes.division', ['allowAll' => true])
        </h3>

        @if (count($fixtures))

            @foreach ($fixtures as $date => $date_fixtures)

                <div class="panel">
                    <h4>{{ (new DateTime($date))->format('F j') }}</h4>
                    <ul>

                        @foreach ($date_fixtures as $fixture)

                            <li>
                                @include('includes.fixture', ['fixture' => $fixture])
                            </li>

                        @endforeach

                    </ul>
                </div>

            @endforeach

        @else

            <p>There are no fixtures to show!</p>

        @endif

    </div>

@endsection
