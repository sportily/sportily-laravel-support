@extends('layouts.main')

@section('content')

    <div class="primary">

        {{ json_encode($in_progress) }}

        @include('snippets.now-playing')

        @include('snippets.upcoming-fixtures')

        @include('snippets.recent-results')

    </div>

    <div class="sidebar">

        @include('snippets.headlines')

        @include('includes.facebook-widget')

    </div>

@endsection
