@extends('layouts.main')

@section('content')

    <h3>Registration <span>| 2016-2017 Season</span></h3>

    <p>
        <strong>Please note:</strong> If you experience any problems whilst
        completing the registration process, please raise a support ticket
        <a href="https://sportily.freshdesk.com/support/tickets/new">here</a>.
    </p>

    <div ng-app="app">
        <sportily-registration season="6" registration="55"
            agreement-message="I agree to abide by BIPHA Rules, Byelaws, Code of Conduct and Child Protection Policy."
            confirmation-message="Thank you for registering to participate in the 2016-2017 Season. We have just sent you a confirmation email. If this is your first season, the confirmation email will contain instructions of how to verify your account to complete the registration process.">
        </sportily-registration>
    </div>

@endsection

@section('scripts')
    @include('includes.angular')
    <script src="{{ asset('components/sportily-angular-registration/dist/js/sportily.registration.all.js?rel=@@timestamp') }}"></script>
    <script>angular.module('app', ['app.core', 'sportily.registration']);</script>
@endsection
