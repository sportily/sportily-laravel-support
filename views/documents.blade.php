@extends('layouts.main')

@section('content')

    <h3>Documents</h3>
    <ul class="list">

        @foreach ($documents as $document)

            <li>
                <a target="_blank" href="{{ $document['file']['uri'] }}">{{ $document['title'] }}</a>
            </li>

        @endforeach

    </ul>

@endsection
