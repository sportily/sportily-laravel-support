@extends('layouts.main')

@section('content')

    @include('includes.divisions', ['route' => 'player-stats'])

    <div class="full fixtures">
        <h3>
            Player Stats
            @include('includes.division')
        </h3>

        @if (isset($division_id))

            <table class="table standings">
                <thead>
                    <tr>
                        <th class="index aux">&nbsp;</th>
                        <th class="title">Player</th>
                        <th align="center">Played</th>
                        <th align="center">Goals</th>
                        <th align="center">Assists</th>
                        <th class="pts">Pts</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($roles as $index => $player)
                        <tr>
                            <td class="index aux">{{ $index + 1 }}.</td>
                            <td class="title">
                                {{ $player['name']['given_name'] }}
                                {{ $player['name']['family_name'] }}
                            </td>
                            <td align="center">{{ $player['stats']['played'] }}</td>
                            <td align="center">{{ $player['stats']['goals'] }}</td>
                            <td align="center">{{ $player['stats']['assists'] }}</td>
                            <td class="pts">{{ $player['stats']['points'] }}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

        @else
            <p>You must choose a division to see player stats.</p>
        @endif
    </div>

@endsection
