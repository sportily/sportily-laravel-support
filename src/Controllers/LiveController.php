<?php
namespace Sportily\Support\Controllers;

use Sportily\OAuth\Facades\OAuth;

class LiveController extends Controller {

    public function getIndex() {
        $access_token = OAuth::getToken();
        return view('live', [
            'access_token' => $access_token,
            'fixture_id' => ''
        ]);
    }

}
