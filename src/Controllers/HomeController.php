<?php
namespace Sportily\Support\Controllers;

use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Sportily\Api\Endpoints\Fixtures;
use Sportily\Api\Endpoints\Galleries;
use Sportily\Api\Endpoints\Posts;

class HomeController extends Controller {

    private $fixtures;

    private $posts;

    private $galleries;

	public function __construct(Fixtures $fixtures, Posts $posts, Galleries $galleries) {
        $this->fixtures = $fixtures;
        $this->posts = $posts;
        $this->galleries = $galleries;
	}

    public function getIndex(Request $request) {
        $context = $request->organisation['active_context'];
        $organisationId = $request->organisation['id'];

        # retrieve a list of posts from the api.
        $posts = $this->getPosts($organisationId);

        # retrieve upcoming fixtures and most recent results.
        $fixtures = $this->getUpcomingFixtures($context);
        $photos = $this->getLatestPhotos($organisationId);
        $results = $this->getLatestFixtures($context);
        $in_progress = $this->getInProgressFixtures($context);

        # render the home view.
        return view('home', [
            'posts' => $posts,
            'fixtures' => $fixtures,
            'results' => $results,
            'in_progress' => $in_progress,
            'photos' => $photos
        ]);
    }

    private function getPosts($organisationId) {
        $key = 'posts?organisation_id=' . $organisationId;

        return Cache::remember($key, 5, function() use($organisationId) {
            return $this->posts->all([
                'organisation_id' => $organisationId,
                'status' => 'published'
            ]);
        });
    }

    private function getLatestPhotos($organisationId) {
      $key = "photos?organisation_id=" . $organisationId;
      return Cache::remember($key, 5, function() use($organisationId) {

        $images = [];

        $galleries = $this->galleries->all(['organisation_id' => $organisationId]);
        foreach ($galleries as $gallery) {
          if (array_key_exists('images', $gallery) && array_key_exists('data', $gallery['images'])) {
            $images = array_merge($images, $gallery['images']['data']);
          }
        }

        usort($images, function($a, $b) {
          $ad = $a['id'];
          $bd = $b['id'];

          if ($ad == $bd) {
            return 0;
          }

          return $ad < $bd ? 1 : -1;
        });

        return array_slice($images, 0, 6);
      });
    }

    private function getUpcomingFixtures($context) {
        if (!$context) {
          return null;
        }
        $key = 'fixtures?period=future&competition_id=' . $context['competition_id'];
        //If context is not set, e.g. no active competiton because there isn't one
        return Cache::remember($key, 5, function() use($context) {
            $query = array_merge($context, ['include' => 'division']);
            return $this->fixtures->future($query)->groupByDate()->first();
        });
    }

    private function getLatestFixtures($context) {
        if (!$context) {
          return null;
        }
        $key = 'fixtures?period=past&competition_id=' . $context['competition_id'];
        return Cache::remember($key, 5, function() use($context) {
            $query = array_merge($context, ['include' => 'division']);
            return $this->fixtures->past($query)->groupByDate()->last();
        });
    }

    private function getInProgressFixtures($context) {
        if (!$context) {
          return null;
        }
        $fixtures = $this->fixtures->inProgress(array_merge($context, ['include' => 'division']));
        return $fixtures->first();
    }

}
