<?php
namespace Sportily\Support\Controllers;

use Sportily\OAuth\Facades\OAuth;
use Illuminate\Http\Request;
use Sportily\Api\Endpoints\Competitions;

class RegistrationController extends Controller {

    private $competitions;

    public function __construct(Competitions $competitions) {
        $this->competitions = $competitions;
    }

    public function getIndex(Request $request) {
        $context = $request->organisation['active_context'];
        $competition = $this->competitions->retrieve($context['competition_id']);
        $access_token = OAuth::getToken();
        return view('registration', [
            'access_token' => $access_token,
            'competition' => $competition
        ]);
    }

}
