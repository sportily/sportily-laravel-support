<?php
namespace Sportily\Support\Controllers;

use Illuminate\Http\Request;
use Sportily\Api\Endpoints\Posts;

class PostsController extends Controller {

    private $posts;

    public function __construct(Posts $posts) {
        $this->posts = $posts;
    }
    public function getIndex(Request $request) {
        $posts = $this->posts->all([
            'organisation_id' => $request->organisation['id'],
            'status' => 'published'
        ]);
        return view('posts', ['posts' => $posts]);
    }

    public function getSingle($id) {
        $post = $this->posts->retrieve($id);
        return view('post', ['post' => $post]);
    }

}
