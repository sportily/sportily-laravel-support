<?php
namespace Sportily\Support\Controllers;
use Sportily\Api\Endpoints\Contacts;
use Illuminate\Http\Request;

class ContactUsController extends Controller {

	private $contacts;
	public function __construct(Contacts $contacts) {
			$this->contacts = $contacts;
	}
	public function getIndex(Request $request) {
		$contacts = $this->contacts->all([
				'organisation_id' => $request->organisation['id']
		]);
		return view('contact-us', ['contacts' => $contacts]);
	}

}
