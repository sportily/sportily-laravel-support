<?php
namespace Sportily\Support\Controllers;

use Sportily\Api\Endpoints\Links;
use Illuminate\Http\Request;

class LinksController extends Controller {

  private $links;

  public function __construct(Links $links) {
    $this->links = $links;
  }

	public function getIndex(Request $request) {
    $links = $this->links->all([
        'organisation_id' => $request->organisation['id']
    ]);
    return view('links', [ 'links' => $links ]);
	}

}
