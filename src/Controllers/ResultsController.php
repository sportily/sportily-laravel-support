<?php
namespace Sportily\Support\Controllers;

use Illuminate\Http\Request;
use Sportily\Api\Endpoints\Fixtures;

class ResultsController extends Controller {

    private $fixtures;

    private static $BASE_FILTERS = [
        'period' => 'past',
        'include' => 'division'
    ];

    public function __construct(Fixtures $fixtures) {
        $this->fixtures = $fixtures;
    }

    public function getIndex(Request $request, $division_id = null) {
        $filter = $division_id == null
            ? $request->organisation['active_context']
            : ['division_id' => $division_id];
        if ($filter) {
          $filter = array_merge($filter, static::$BASE_FILTERS);
          $results = $this->fixtures->all($filter)->groupByDate()->reverse();
        } else {
          $filter = null;
          $results = null;
        }
        return view('results', [
            'filter' => $filter,
            'results' => $results,
            'division_id' => $division_id
        ]);
    }

}
