<?php
namespace Sportily\Support\Controllers;

use Sportily\Api\Endpoints\Galleries;
use Illuminate\Http\Request;

class PhotosController extends Controller {

	private $galleries;

	public function __construct(Galleries $galleries) {
		$this->galleries = $galleries;
	}

	public function getIndex(Request $request) {
		$organisationId = $request->organisation['id'];
		$galleries = $this->galleries->all(['organisation_id' => $organisationId]);
		return view('photos', ['galleries' => $galleries]);
	}

}
