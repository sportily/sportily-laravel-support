<?php
namespace Sportily\Support\Controllers;

use Illuminate\Http\Request;
use Sportily\Api\Endpoints\Documents;

class DocumentsController extends Controller {

    private $documents;

    public function __construct(Documents $documents) {
        $this->documents = $documents;
    }
    public function getIndex(Request $request) {
        $documents = $this->documents->all([
            'organisation_id' => $request->organisation['id'],
            'status' => 'published'
        ]);
        return view('documents', ['documents' => $documents]);
    }

    public function getSingle($id) {
        $document = $this->documents->retrieve($id);
        return view('document', ['document' => $document]);
    }

}
