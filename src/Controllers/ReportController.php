<?php
namespace Sportily\Support\Controllers;

use Sportily\OAuth\Facades\OAuth;

class ReportController extends Controller {

    public function getIndex($fixture_id) {
        $access_token = OAuth::getToken();
        return view('report', [
            'access_token' => $access_token,
            'fixture_id' => $fixture_id
        ]);
    }

}
