<?php
namespace Sportily\Support\Controllers;

use Illuminate\Http\Request;
use Sportily\Api\Endpoints\DivisionEntries;

class TeamStandingsController extends Controller {

    private $divisionEntries;

    private static $BASE_FILTERS = [ 'include' => 'stats' ];

    public function __construct(DivisionEntries $divisionEntries) {
        $this->divisionEntries = $divisionEntries;
    }

    public function getIndex(Request $request, $division_id = null) {
        $filter = [];
        $entries = null;

        if ($division_id != null) {
            $filter = ['division_id' => $division_id];
            $filter = array_merge(static::$BASE_FILTERS, $filter);
            $entries = $this->divisionEntries->all($filter);
        }

        return view('team-standings', [
            'filter' => $filter,
            'entries' => $entries,
            'division_id' => $division_id
        ]);
    }

}
