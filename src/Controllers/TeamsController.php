<?php
namespace Sportily\Support\Controllers;

use Illuminate\Support\Facades\Input;
use Sportily\Api\Endpoints\Teams;
use Sportily\Api\Endpoints\Clubs;
use Sportily\Api\Endpoints\Divisions;
use Sportily\Api\Endpoints\DivisionEntries;
use Sportily\Api\Endpoints\Fixtures;
use Sportily\Api\Endpoints\Roles;

class TeamsController extends Controller {

    private $fixtures;
    private $clubs;
    private $teams;
    private $divisions;
    private $divisionEntries;
    private $role;

	public function __construct(Teams $teams, Clubs $clubs, Divisions $divisions, DivisionEntries $divisionEnties, Fixtures $fixtures, Roles $roles) {
        $this->fixtures = $fixtures;
        $this->teams = $teams;
        $this->divisions = $divisions;
        $this->divisionEntries = $divisionEnties;
        $this->clubs = $clubs;
        $this->roles = $roles;
	}

    public function getIndex($team_id, $entry_id) {
        $competition_id = Input::get('organisation.active_competition_id');
        # fetch the team and club details.
        $team = $this->teams->retrieve($team_id);
        $club = $this->clubs->retrieve($team['club_id'], ['include' => 'logo_image']);

        # fetch a list of all divisions in the competition.
        $team_entries = $this->divisionEntries->all([ 'team_id' => $team_id ]);
        $divisions = $this->divisions->all([ 'competition_id' => $competition_id ]);

        # fetch the division entry details.
        $entry = $this->divisionEntries->retrieve($entry_id);
        $entries = $this->divisionEntries->all([
            'division_id' => $entry['division_id'],
            'include' => 'stats'
        ])->keyBy('id');

        # lookup the division of the entry.
        $division = $this->divisions->retrieve($entry['division_id']);

        # fetch the team's fixtures.
        $fixtures = $this->fixtures->all([
            'team_id' => $entry['id'],
            'period' => 'future',
            'include' => 'venue'
        ]);

        # fetch the team's results.
        $results = $this->fixtures->all([
            'team_id' => $entry['id'],
            'period' => 'past',
            'include' => 'venue'
        ]);

        # fetch all of the roles for the age group.
        $players = $this->roles->all([
            'competition_id' => $competition_id,
            'age_group_id' => $team['age_group_id'],
            'type' => 'player',
            'include' => 'stats'
        ]);
        # generate the team stats.
        $stats = $this->generateStats($entry_id, $results, $entries);

        # generate the player stats.
        $players = $this->filterTeamRoles($players, $team_id);

        # filter the team entries to those that are relevant for the comp.
        $team_entries = $this->filterTeamEntries($team_entries, $divisions);

        return view('team', [
            'club' => $club,
            'division' => $division,
            'entries' => $entries,
            'fixtures' => $fixtures,
            'players' => $players,
            'results' => $results,
            'stats' => $stats,
            'team' => $team,
            'team_entries' => $team_entries
        ]);
    }

    private function generateStats($id, $results, $entries) {
        $entry = $entries->get($id);
        $stats = $entry['stats'];

        if ($stats['played'] > 0) {
            $stats['win_percentage'] = $stats['won'] / $stats['played'] * 100;
            $stats['goal_average'] = $stats['goals_for'] / $stats['played'];
        } else {
            $stats['win_percentage'] = 0;
            $stats['goal_average'] = 0;
        }

        $stats['clean_sheets'] = 0;
        $stats['best_result'] = [ 'gf' => null, 'ga' => null, 'gd' => null ];

        array_map(function($result) use(&$stats, $id) {
            # only consider fixtures that are complete.
            if ($result['status'] == 'complete') {

                # determine which of the entries is the team we care about.
                $isHomeTeam = $result['home_entry']['division_entry_id'] == $id;
                $for = $result[$isHomeTeam ? 'home_entry' : 'away_entry'];
                $against = $result[$isHomeTeam ? 'away_entry' : 'home_entry'];

                # goals for and against the team we care about.
                if ($for['result']['type'] == 'standard/scores'
                    && $against['result']['type'] == 'standard/scores') {

                    $gf = $for['result']['score'];
                    $ga = $against['result']['score'];
                    $gd = $gf - $ga;

                    # better goal difference than the best?
                    $bestGoalDiff = $stats['best_result']['gd'];
                    if ($bestGoalDiff == null || $gd > $bestGoalDiff) {
                        $stats['best_result'] = [ 'gf' => $gf, 'ga' => $ga, 'gd' => $gd ];
                    }

                    # equal goal diff, but more goals scored?
                    $bestGoalsFor = $stats['best_result']['gf'];
                    if ($gd == $bestGoalDiff && $gf > $bestGoalsFor) {
                        $stats['best_result'] = [ 'gf' => $gf, 'ga' => $ga, 'gd' => $gd ];
                    }

                    # clean sheets.
                    if ($ga == 0) {
                        $stats['clean_sheets']++;
                    }
                }

            }

        }, $results->all());

        return $stats;
    }

    private function filterTeamRoles($roles, $team_id) {
        return array_values(array_filter($roles->all(), function($role) use($team_id) {
            return $role['team_id'] == $team_id;
        }));
    }

    private function filterTeamEntries($team_entries, $divisions) {
        return array_filter($team_entries->all(), function($entry) use($divisions) {
            return isset($divisions->lookup[$entry['division_id']]);
        });
    }

}
