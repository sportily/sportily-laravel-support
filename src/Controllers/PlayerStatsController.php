<?php
namespace Sportily\Support\Controllers;

use Illuminate\Http\Request;
use Sportily\Api\Endpoints\Roles;

class PlayerStatsController extends Controller {

    private $roles;

    private static $BASE_FILTERS = [ 'type' => 'player', 'include' => 'stats' ];

    public function __construct(Roles $roles) {
        $this->roles = $roles;
    }

    public function getIndex(Request $request, $division_id = null) {
        $filter = [];
        $roles = null;

        if ($division_id != null) {
            $filter = ['division_id' => $division_id];
            $filter = array_merge(static::$BASE_FILTERS, $filter);
            $roles = $this->roles->all($filter);
        }

        return view('player-stats', [
            'filter' => $filter,
            'roles' => $roles,
            'division_id' => $division_id
        ]);
    }

}
