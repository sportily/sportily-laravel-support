<?php
namespace Sportily\Support\Controllers;

use Illuminate\Http\Request;
use Sportily\Api\Endpoints\Fixtures;
use Sportily\Api\Endpoints\Venues;

class FixturesController extends Controller {

    private $fixtures;

    private $venues;

    private static $BASE_FILTERS = [
        'period' => 'future',
        'include' => 'division'
    ];

    public function __construct(Fixtures $fixtures, Venues $venues) {
        $this->fixtures = $fixtures;
        $this->venues = $venues;
    }

    public function getIndex(Request $request, $division_id = null) {
        $filter = $division_id == null
            ? $request->organisation['active_context']
            : ['division_id' => $division_id];

        if ($filter) {
          $filter = array_merge($filter, static::$BASE_FILTERS);
          $fixtures = $this->fixtures->all($filter)->groupByDate();
        } else {
          $filter = null;
          $fixtures = null;
        }
        return view('fixtures', [
            'filter' => $filter,
            'fixtures' => $fixtures,
            'division_id' => $division_id
        ]);
    }

    public function live(Request $request, $fixture_id) {
        # fetch the fixture details.
        $fixture = $this->fixtures->retrieve($fixture_id);

        # fetch venue details.
        $venue = $fixture['venue_id'] != null
            ? $this->venues->retrieve($fixture['venue_id'])
            : null;

        # render the timeline.
        return view('fixture-live', [
            'fixture' => $fixture,
            'venue' => $venue,
            'access_token' => $request->session()->get('access_token')->value
        ]);
    }

}
