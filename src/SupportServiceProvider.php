<?php
namespace Sportily\Support;

use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

/**
 *
 */
class SupportServiceProvider extends ServiceProvider {

    /**
     * Publish configuration.
     */
    public function boot(Router $router) {
        $this->bootConfiguration();
        $this->bootRoutes($router);
        $this->bootViews();
        $this->bootHelpers();
    }

    public function bootConfiguration() {
        $source = realpath(__DIR__ . '/../config/sportily-support.php');

        if ($this->app instanceof LaravelApplication && $this->app->runningInConsole()) {
            $this->publishes([$source => config_path('sportily-support.php')]);
        }

        $this->mergeConfigFrom($source, 'sportily-support');
    }

    private function bootRoutes($router) {
        $config = [
            'middleware' => ['web', 'sportily.public'],
            'namespace' => 'Sportily\Support\Controllers'
        ];

        $router->group($config, function($router) {
            $router->get('/', [
                'as' => 'home',
                'uses' => 'HomeController@getIndex'
            ]);
            $router->get('news/', [
                'as' => 'posts',
                'uses' => 'PostsController@getIndex'
            ]);
            $router->get('news/{post_id?}', [
                'as' => 'post',
                'uses' => 'PostsController@getSingle'
            ]);
            $router->get('documents/', [
                'as' => 'documents',
                'uses' => 'DocumentsController@getIndex'
            ]);
            $router->get('documents/{post_id?}', [
                'as' => 'document',
                'uses' => 'DocumentsController@getSingle'
            ]);
            $router->get('fixtures/{division_id?}', [
                'as' => 'fixtures',
                'uses' => 'FixturesController@getIndex'
            ]);
            $router->get('fixtures/{fixture_id}/live', [
                'as' => 'fixture-live',
                'uses' => 'FixturesController@live'
            ]);
            $router->get('results/{division_id?}', [
                'as' => 'results',
                'uses' => 'ResultsController@getIndex'
            ]);
            $router->get('team-standings/{division_id?}', [
                'as' => 'team-standings',
                'uses' => 'TeamStandingsController@getIndex'
            ]);
            $router->get('player-stats/{division_id?}', [
                'as' => 'player-stats',
                'uses' => 'PlayerStatsController@getIndex'
            ]);
            $router->get('teams/{team_id}/{entry_id}', [
                'as' => 'team',
                'uses' => 'TeamsController@getIndex'
            ]);
            $router->get('links/', [
                'as' => 'links',
                'uses' => 'LinksController@getIndex'
            ]);
            $router->get('report/{fixture_id}', [
                'as' => 'report',
                'uses' => 'ReportController@getIndex'
            ]);
            $router->get('live', [
                'as' => 'live',
                'uses' => 'LiveController@getIndex'
            ]);
            $router->get('photos', [
              'as' => 'photos',
              'uses' => 'PhotosController@getIndex'
            ]);
            $router->get('contact-us', [
              'as' => 'contact-us',
              'uses' => 'ContactUsController@getIndex'
            ]);
            $router->get('registration', [
              'as' => 'registration',
              'uses' => 'RegistrationController@getIndex'
            ]);

        });
    }

    public function bootViews() {
        view()->addLocation(realpath(__DIR__ . '/../views'));

        view()->composer('*', Composers\OrganisationComposer::class);
        view()->composer('*', Composers\ProfileComposer::class);

        view()->composer(
            ['fixtures', 'results', 'team-standings', 'player-stats'],
            Composers\DivisionsComposer::class
        );
    }

    public function bootHelpers() {
        $helpers = realpath(__DIR__ . '/../helpers/helpers.php');
        require_once($helpers);
    }

    public function register() {
        // nothing to register.
    }

}
