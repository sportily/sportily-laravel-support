<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;
use Sportily\Api\Endpoints\Organisations;
use Illuminate\Support\Facades\Cache;

abstract class AbstractDatabaseBasedOrganisationId {

    protected $organisations;

    public function __construct(Organisations $organisations) {
        $this->organisations = $organisations;
    }

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        $subdomain = $this->getSubdomain($request);
        $organisation_id = $this->getOrganisationId($subdomain);

        if ($organisation_id) {
            $request->merge(['organisation_id' => $organisation_id]);
        }

        return $next($request);
    }

    protected function getOrganisationId($subdomain) {
        $organisation_id = null;
        $domains = Cache::get('sportily_domains');

        if (is_array($domains) && array_key_exists($subdomain, $domains)) {
            $organisation_id = $domains[$subdomain];
        } else {
            $organisations = $this->organisations->all(['subdomain' => $subdomain]);
            if (count($organisations) > 0) {
                $organisation_id = $organisations->first()['id'];
                if (!is_array($domains)) {
                    $domains = [];
                }
                $domains[$subdomain] = $organisation_id;
                Cache::put('sportily_domains', $domains);
            }
        }

        return $organisation_id;
    }

    abstract protected function getSubdomain($request);

}
