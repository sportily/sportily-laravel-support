<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;

class PrefixBasedOrganisationId {

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        $segment = $request->segment(1);
  
        $organisations = Config::get('sportily-support.organisations');
        if (isset($organisations[$segment])) {
            $dup = $request->duplicate();
            $dup->merge(['organisation_id' => $organisations[$segment]]);
            $dup->merge(['__prefix' => $segment]);
            $dup->server->set('REQUEST_URI', str_replace($segment, '', $request->path()));
            return $next($dup);
        }

        return $next($request);
    }

}
