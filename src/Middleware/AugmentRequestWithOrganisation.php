<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;
use Sportily\Api\Endpoints\Organisations;

class AugmentRequestWithOrganisation {

    /** API endpoint for organisations. */
    private $organisations;

    /**
     * Construct a new middleware instance.
     */
    public function __construct(Organisations $organisations) {
        $this->organisations = $organisations;
    }

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        $organisation = $this->organisations->retrieve($request->organisation_id);
        $request->merge(['organisation' => $organisation]);
        
        return $next($request);
    }

}
