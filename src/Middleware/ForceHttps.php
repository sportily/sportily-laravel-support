<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;


class ForceHttps {

    public function handle($request, Closure $next) {
        //get the urls that we want to force to unsecure
        $forceHttp = Config::get('sportily-support.forceHttp', []);
        if (!$request->secure() && $this->forceSecureForEnv() && !in_array($request->path(), $forceHttp)) {
            return redirect()->secure($request->getRequestUri());
        }

        //force the http routes
        if ($request->secure() && in_array($request->path(), $forceHttp)) {
            return redirect()->to($request->path(), 302, [], false);
        }

        return $next($request);
    }

    private function forceSecureForEnv() {
        $secureEnvs = Config::get('sportily-support.secure-envs', []);
        return in_array(env('APP_ENV'), $secureEnvs);
    }
}

?>
