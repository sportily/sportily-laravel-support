<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;

class HostBasedOrganisationId {

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        $host = $request->getHost();
        $organisations = Config::get('sportily-support.organisations');

        if (isset($organisations[$host])) {
            $request->merge(['organisation_id' => $organisations[$host]]);
        }

        return $next($request);
    }

}
