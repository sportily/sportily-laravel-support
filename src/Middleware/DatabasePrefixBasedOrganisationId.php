<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;
use Illuminate\Support\Facades\Cache;

class DatabasePrefixBasedOrganisationId extends DatabaseHostBasedOrganisationId {

  /**
   * Handle the request.
   */
  public function handle($request, Closure $next) {
      $segment = $this->getSubdomain($request);
      $organisation_id = $this->getOrganisationId($segment);

      if ($organisation_id) {
          $dup = $request->duplicate();
          $dup->merge(['organisation_id' => $organisation_id]);
          $dup->merge(['__prefix' => $segment]);
          $dup->server->set('REQUEST_URI', str_replace($segment, '', $request->path()));
          return $next($dup);
      }

      return $next($request);
  }

    /**
     * get the 'subdomain' from the uri
     */
    protected function getSubdomain($request) {
        return $request->segment(1);
    }

}
