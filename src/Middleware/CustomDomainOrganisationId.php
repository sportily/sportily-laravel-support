<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;

class CustomDomainOrganisationId {

      /**
       * Handle the request.
       */
      public function handle($request, Closure $next) {
          $host = $request->getHost();
          if ($host == 'www.brha.co' || $host == 'brha.co') {
            $request->merge(['organisation_id' => 36]);

          }
          return $next($request);
      }
}
