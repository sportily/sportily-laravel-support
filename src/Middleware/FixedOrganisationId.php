<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;

class FixedOrganisationId {

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        $request->merge([
            'organisation_id' => Config::get('sportily-support.organisation')
        ]);

        return $next($request);
    }

}
