<?php
namespace Sportily\Support\Middleware;

use Closure;
use Config;

class DatabaseHostBasedOrganisationId extends AbstractDatabaseBasedOrganisationId {

    /**
     * get the 'subdomain' from the host
     */
    protected function getSubdomain($request) {
        $host = $request->getHost();
        $subdomain = str_replace(Config::get('sportily-support.domain'), '', $host);

        return $subdomain;
    }

}
