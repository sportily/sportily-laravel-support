<?php
namespace Sportily\Support\Composers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Sportily\Api\Endpoints\Divisions;

class DivisionsComposer {

    private $request;

    private $divisions;

    public function __construct(Request $request, Divisions $divisions) {
        $this->request = $request;
        $this->divisions = $divisions;
    }

    public function compose(View $view) {
        $filter = $this->request->organisation['active_context'];
        if ($filter) {
          $divisons = $this->divisions->all($filter)->keyBy('id');
        } else {
          $divisons = null;
        }
        $view->with('divisions', $divisons);
    }

}
