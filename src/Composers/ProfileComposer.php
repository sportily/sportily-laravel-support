<?php
namespace Sportily\Support\Composers;

use Illuminate\View\View;
use Sportily\Api\Endpoints\Profile;

class ProfileComposer {

    private $profile;

    public function __construct(Profile $profile) {
        $this->profile = $profile;
    }

    public function compose(View $view) {

        $view->with('profile', null);//$this->profile->get());
    }

}
