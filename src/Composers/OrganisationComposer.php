<?php
namespace Sportily\Support\Composers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class OrganisationComposer {

    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function compose(View $view) {
        $view->with('organisation', $this->request->organisation);
    }

}
