# Sportily Support library for Laravel

## Installation

Composer is the recommended way to install this package. Add the following line to your `composer.json` file:

```json
"sportily/sportily-laravel-support": "dev-master@dev"
```

Then run `composer update` to get the package.

Once composer has installed the package this the following line to the `providers` array, located in your `config/app.php` file:

```php
Sportily\Support\SupportServiceProvider::class,
```

Next, run `php artisan vendor:publish` to publish this package's configuration.

## Middleware

### Forcing HTTPS in certain environments

We recommended all production environments run over a https connection, to do so register the middleware in Kernel.php under 'web'

```php
'web' => [
    \App\Http\Middleware\EncryptCookies::class,
    ...
    ...
    \Sportily\Support\Middleware\ForceHttps::class,
],
```

Add to sportily-support.php the environments to secure.

```php
    'secure-envs' => ['prod', 'production']
```

### Obtaining an organisation ID

To make use of the organisation id in Controller methods, it can be plucked out of the Request like in the example below.

```php
public function getIndex(Request $request) {
    $posts = $this->posts->all([
        'organisation_id' => $request->organisation['id'],
        'status' => 'published'
    ]);
    return view('posts', ['posts' => $posts]);
}
```

#### Fixed ID

#### URL Prefix based ID

#### Host based ID

Register the HostBasedOrganisationId in the middleware groups require-dev

```php
'sportily.private' => [
    \Sportily\OAuth\Middleware\OAuthMiddlewarePrivate::class,
    \Sportily\Support\Middleware\HostBasedOrganisationId::class,
    \Sportily\Support\Middleware\AugmentRequestWithOrganisation::class,
],

'sportily.public' => [
    \Sportily\OAuth\Middleware\OAuthMiddlewarePublic::class,
    \Sportily\Support\Middleware\HostBasedOrganisationId::class,
    \Sportily\Support\Middleware\AugmentRequestWithOrganisation::class,
]
```
